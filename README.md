## Marvin - A ChatGPT-powered Slack bot

This project consists of two lambdas which talk via an SNS topic.

* https://gitlab.com/rosco.peco/chatgpt-slack-replier (this project)
* https://gitlab.com/rosco.peco/chatgpt-slack-handler

You'll need to deploy both and set up the topic with the relevant 
permissions if you want to deploy this yourself.

The reason for the split is that Slack has a hard limit of three seconds
for responding, which isn't long enough for an OpenAI API request
(it often isn't even enough to spin up this lambda!).

This is the second lambda - It picks up the message and slack context
from SNS, does the OpenAI request, and then posts back to slack.