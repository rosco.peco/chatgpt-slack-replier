package com.xdesign.rossbamford.chatgpt

import com.fasterxml.jackson.databind.ObjectMapper
import com.xdesign.rossbamford.marvin.net.WebClient
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ChatGPTClientTest {
    private var httpClient: WebClient? = null
    private var objectMapper: ObjectMapper? = null
    private var client: ChatGPTClient? = null

    @BeforeEach
    fun setup() {
        httpClient = mockk()
        objectMapper = mockk()

        client = ChatGPTClient(httpClient!!, objectMapper!!)
    }

    private fun httpClient() = httpClient!!
    private fun objectMapper() = objectMapper!!
    private fun client() = client!!

    @Test
    fun `Constructs request, calls http client and transforms response correctly`() {
        val testPrompt = "Hello?"

        val expectedPrompt = ChatGptPrompt("gpt-4", listOf(
            ChatGptMessage(role = "system", content = "You are a helpful but very sarcastic assistant named Marvin."),
            ChatGptMessage(role = "user", testPrompt))
        )

        val expectedResponse = testResponse()

        every { objectMapper().writeValueAsString(eq(expectedPrompt)) }.returns("{ \"test\": true }")
        every { httpClient().exchangeJSON(any(), any(), any()) }.returns("{\"whatever\": 42}")
        every { objectMapper().readValue("{\"whatever\": 42}", ChatGPTResponse::class.java) }.returns(expectedResponse)

        val result = client().prompt(testPrompt)

        assertEquals(expectedResponse, result)
    }

    private fun testResponse() = ChatGPTResponse("test-id", "test-object", 1001, "test-model",
        ChatGPTUsage(9000, 9001), listOf(
            ChatGPTChoice(ChatGptMessage("assistant", "Yolo"), "test-reason", 100)
        )
    )
}