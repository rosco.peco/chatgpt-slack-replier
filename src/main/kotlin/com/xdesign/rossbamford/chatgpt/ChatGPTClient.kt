package com.xdesign.rossbamford.chatgpt

import com.fasterxml.jackson.databind.ObjectMapper
import com.xdesign.rossbamford.marvin.net.WebClient
import jakarta.inject.Inject
import jakarta.inject.Singleton
import org.slf4j.LoggerFactory

@Singleton
class ChatGPTClient() {
    private val key = System.getenv().getOrDefault("OPENAI_API_KEY", "DEFAULT_VALUE_FOR_TESTS")
    private val uri = "https://api.openai.com/v1/chat/completions"

    @Inject
    lateinit var http: WebClient

    @Inject
    lateinit var objectMapper: ObjectMapper

    // For test use only
    internal constructor(http: WebClient, objectMapper: ObjectMapper) : this() {
        this.http = http
        this.objectMapper = objectMapper
    }

    fun prompt(p: String): ChatGPTResponse {
        LOG.debug("ChatGPTClient#prompt: {}", p)

        return objectMapper.readValue(
            http.exchangeJSON(ChatGptPrompt(p), uri) { b -> b.header("Authorization", "Bearer $key") },
            ChatGPTResponse::class.java
        )
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(ChatGPTClient::class.java)
    }
}