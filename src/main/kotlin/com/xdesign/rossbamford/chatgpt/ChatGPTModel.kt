package com.xdesign.rossbamford.chatgpt

import com.fasterxml.jackson.annotation.JsonProperty

data class ChatGptMessage(
    @JsonProperty val role: String,
    @JsonProperty val content: String
)

data class ChatGptPrompt(
    @JsonProperty val model: String,
    @JsonProperty val messages: List<ChatGptMessage>,
) {
    constructor(prompt: String) : this("gpt-4", listOf(
        ChatGptMessage("system", "You are a helpful but very sarcastic assistant named Marvin."),
        ChatGptMessage("user", prompt)
    ))
}

data class ChatGPTUsage(
    @JsonProperty("prompt_tokens") val promptTokens: Int,
    @JsonProperty("completion_tokens") val completionTokens: Int
)

data class ChatGPTChoice(
    @JsonProperty val message: ChatGptMessage,
    @JsonProperty("finish_reason") val finishReason: String?,
    @JsonProperty val index: Int
)

data class ChatGPTResponse(
    @JsonProperty val id: String,
    @JsonProperty val `object`: String,
    @JsonProperty val created: Long,
    @JsonProperty val model: String,
    @JsonProperty val usage: ChatGPTUsage,
    @JsonProperty val choices: List<ChatGPTChoice>
)
