package com.xdesign.rossbamford.marvin.replier

import com.fasterxml.jackson.annotation.JsonProperty

internal data class SnsPrompt(
    @JsonProperty val prompt: String,
    @JsonProperty val userId: String,
    @JsonProperty val replyUrl: String
)

internal data class SlackReply(val response: String, val replyUrl: String)