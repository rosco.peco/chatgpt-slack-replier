package com.xdesign.rossbamford.marvin.replier

import com.fasterxml.jackson.annotation.JsonProperty
import com.xdesign.rossbamford.marvin.net.WebClient
import jakarta.inject.Inject
import jakarta.inject.Singleton
import org.slf4j.LoggerFactory

internal data class SlashCommandMetadata(
    val eventType: String = "",
    val eventPayload: Map<String, Any> = emptyMap()

)
internal data class SlashCommandResponse(
    @JsonProperty("response_type") val responseType: String,
    @JsonProperty val text: String,
    @JsonProperty val attachments: List<String> = emptyList(),
    @JsonProperty val blocks: List<String> = emptyList(),
    @JsonProperty val metadata: SlashCommandMetadata = SlashCommandMetadata()
) {
    constructor(reply: SlackReply) : this(responseType = "in_channel", text = reply.response)
}

@Singleton
internal class SlackReplier {
    @Inject
    lateinit var webClient: WebClient

    fun reply(reply: SlackReply) {
        LOG.debug("Replying: {} to {}", reply.response.take(20), reply.replyUrl)

        val response = try {
            webClient.exchangeJSON(SlashCommandResponse(reply), reply.replyUrl)
        } catch (e: Exception) {
            LOG.error("Failed to communicate with either OpenAI or Slack", e)
            "<error:${e.message}"
        }

        LOG.trace(">>>>> Response: {}", response)
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(SlackReplier::class.java)
    }
}