package com.xdesign.rossbamford.marvin.replier

import com.amazonaws.services.lambda.runtime.events.SNSEvent
import com.fasterxml.jackson.databind.ObjectMapper
import com.xdesign.rossbamford.chatgpt.ChatGPTChoice
import com.xdesign.rossbamford.chatgpt.ChatGPTClient
import io.micronaut.function.aws.MicronautRequestHandler
import jakarta.inject.Inject
import org.slf4j.LoggerFactory

class FunctionRequestHandler : MicronautRequestHandler<SNSEvent, Any?>() {
    @Inject
    private lateinit var objectMapper: ObjectMapper

    @Inject
    private lateinit var chatGPTClient: ChatGPTClient

    @Inject
    private lateinit var slackReplier: SlackReplier

    override fun execute(input: SNSEvent): Any? {
        LOG.info("Received SNS: {}", input)

        input.records
            ?.map { objectMapper.readValue(it.sns.message, SnsPrompt::class.java) }
            ?.map { snsPrompt -> handlePrompt(snsPrompt) }
            ?.forEach { response -> slackReplier.reply(response) }

        return null
    }

    private fun handlePrompt(snsPrompt: SnsPrompt): SlackReply {
        val promptResponse = chatGPTClient.prompt(snsPrompt.prompt)
        val replies = extractReplies(promptResponse.choices)
        return SlackReply("<@${snsPrompt.userId}> $replies", snsPrompt.replyUrl)
    }

    private fun extractReplies(choices: List<ChatGPTChoice>?) = choices?.joinToString { it.message.content } ?: "<Something went wrong>"

    companion object {
        private val LOG = LoggerFactory.getLogger(FunctionRequestHandler::class.java)
    }
}