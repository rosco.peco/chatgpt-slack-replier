package com.xdesign.rossbamford.marvin.net

import java.net.http.HttpRequest

interface WebClient {
    fun exchangeJSON(obj: Any, url: String, requestTap: (HttpRequest.Builder) -> HttpRequest.Builder): String
    fun exchangeJSON(obj: Any, url: String) = exchangeJSON(obj, url) { it }
}