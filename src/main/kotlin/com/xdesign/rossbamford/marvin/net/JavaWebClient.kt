package com.xdesign.rossbamford.marvin.net

import com.fasterxml.jackson.databind.ObjectMapper
import io.micronaut.context.annotation.Bean
import io.micronaut.http.MediaType
import jakarta.inject.Inject
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

@Bean
class JavaWebClient : WebClient {
    @Inject
    lateinit var objectMapper: ObjectMapper

    @Inject
    lateinit var httpClient: HttpClient

    override fun exchangeJSON(obj: Any, url: String, requestTap: (HttpRequest.Builder) -> HttpRequest.Builder): String {
        val req = HttpRequest.newBuilder()
            .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(obj)))
            .uri(URI.create(url))
            .header("Content-Type", MediaType.APPLICATION_JSON)
            .header("Accept", MediaType.APPLICATION_JSON)
            .let { requestTap(it) }
            .build()

        return httpClient.send(req, HttpResponse.BodyHandlers.ofString()).body()
    }
}