package com.xdesign.rossbamford.marvin.net

import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory
import java.net.http.HttpClient
import java.time.Duration

@Factory
class ContextFactory {
    @Bean
    fun httpClient(): HttpClient {
        return HttpClient.newBuilder()
            .followRedirects(HttpClient.Redirect.NORMAL)
            .connectTimeout(Duration.ofSeconds(10))
            .build()
    }
}